/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50140
Source Host           : localhost:3306
Source Database       : chart

Target Server Type    : MYSQL
Target Server Version : 50140
File Encoding         : 65001

Date: 2012-04-26 06:20:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `character_bonus_stats`
-- ----------------------------
DROP TABLE IF EXISTS `character_bonus_stats`;
CREATE TABLE `character_bonus_stats` (
  `guid` int(11) unsigned NOT NULL DEFAULT '0',
  `str` int(10) unsigned NOT NULL DEFAULT '0',
  `agl` int(10) unsigned NOT NULL DEFAULT '0',
  `stam` int(10) unsigned NOT NULL DEFAULT '0',
  `intel` int(10) unsigned NOT NULL DEFAULT '0',
  `spt` int(10) unsigned NOT NULL DEFAULT '0',
  `spd` int(10) unsigned NOT NULL DEFAULT '0',
  `up_count` int(10) NOT NULL,
  `played_ups` int(10) NOT NULL,
  `spent_ups` int(10) NOT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of character_bonus_stats
-- ----------------------------
INSERT INTO `character_bonus_stats` VALUES ('1', '10000', '10000', '10000', '10000', '10000', '9040', '0', '0', '0');
