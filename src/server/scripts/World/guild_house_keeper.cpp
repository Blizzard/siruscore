/* Copyright (C) 2008-2010 Scourge <https://scourge.su/>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

/*
This is patch on Guild Houses, only for server Scourge. Special. By Destros.
entry - ID of guildhouse
guild - ID of guild
status - 1 - already buying, 0 - is empty
next_buy - 1 mean, that this gh ready for buying
map, coord_x,y,z,o - coordinates of guildhouse
name and description - description of guildhouse and it's name
===========
point more info about gh in npc_texts with id '54731'. Here can be current gh and future gh. Its appear on gossip menu "Guild House? What is it?".
*/

#include "ScriptPCH.h"

// gossip menu
#define GOSSIP_BUY_GH                         "I want to buy Guild House."
#define GOSSIP_SALE_GH                        "I want to sell Guild House."
#define GOSSIP_SALE_GH_READY                  "I am sure to sell a Guild House."
#define GOSSIP_UPGRADE_GH                     "I want to upgrade my Guild House for 1 level up."
#define GOSSIP_TELEPORT_TO_GH                 "I want to teleport in Guild House."
#define GOSSIP_NEXT_PAGE                      "Next Page -->"
#define GOSSIP_PREV_PAGE                      "<-- Prev Page"
//#define GOSSIP_WHAT_IS_GH                     "Guild House? What is it?"
#define GOSSIP_GOODBYE                        "Goodbye."
#define GOSSIP_OK                             "Okay, thanks."
#define GOSSIP_NO                             "No, thanks."
#define GOSSIP_BUY_TELE_KEY                   "I need a teleport key to Guild House."
// whisper to player
#define WHISPER_NOT_GH_BUY                    "$N, your guild doesn't have a Guild House."
#define WHISPER_NO_REQUERED_LEVEL             "$N, you must have 2 level of Guild House to teleport in Guild House with item."
#define WHISPER_CONGRATULATION_BUY            "Congratulation, $N! You bought a Guild House!"
#define WHISPER_CONGRATULATION_LEVEL          "Congratulation, $N! You upgrade your Guild House!"
#define WHISPER_YOU_ARE_TELEPORTING_TO_GH     "Good Bye $N! Good luck in you Guild House."
#define WHISPER_NO_MONEY                      "You have so little gold for buying it, $N."
#define WHISPER_SALE_GH                       "It's very strange that you sold you Guild House, $N..."
#define WHISPER_NOTHING_GH                    "There are no Guild House available."
#define WHISPER_HIGH_LEVEL                    "You guild so cool!"
#define WHISPER_LOW_GH_LEVEL                  "Level of you Guild House is low for it!"
#define WHISPER_BUY_KEY                       "With help of this key you can teleport to you Guild House from anywhere!"
#define WHISPER_ALREADY_HAS_KEY               "You have already have this key!"
// actions
#define ACTION_BUY                            1
#define ACTION_SELL                           2
#define ACTION_SELL_READY                     3
#define ACTION_UPGRADE                        4
#define ACTION_TELEPORT                       5
#define ACTION_GOODBYE                        6
#define ACTION_OK                             7
#define ACTION_BUY_KEY                        8
#define ACTION_NEXT_PAGE                      9
#define ACTION_PREV_PAGE                      10
// other
#define NUMBER_TO_ID                          1000
#define NUMBER_FOLLOW                         100
#define MAX_GH_LEVEL                          4

#define GH_COST_LEVEL_1     50000*GOLD     // cost of guild house
#define GH_COST_LEVEL_2     150000*GOLD
#define GH_COST_LEVEL_3     300000*GOLD
#define GH_COST_LEVEL_4     500000*GOLD
#define GH_UPDATE_TO_LVL_2  GH_COST_LEVEL_2 - GH_COST_LEVEL_1
#define GH_UPDATE_TO_LVL_3  GH_COST_LEVEL_3 - GH_COST_LEVEL_2
#define GH_UPDATE_TO_LVL_4  GH_COST_LEVEL_4 - GH_COST_LEVEL_3
#define GH_COST_TELEPORT    5*GOLD         // this is normal price - 5 gold - for teleport

void StartGH(Player *pPlayer, Creature *pCreature);

// Variables for function ShowBuyList
uint32 pages=0;
uint32 current_page=0;

bool ShowBuyList(Player *pPlayer, Creature *pCreature, uint32 showFromId = 0)
{

    QueryResult result = CharacterDatabase.PQuery("SELECT entry, name, gh_level FROM guildhouses WHERE next_buy = '1'");
    if (result)
    {
        uint32 guildhouseId = 0;
        std::string name = "";

        pages = result->GetRowCount()/MAX_GH_LEVEL;
        if(result->GetRowCount() > pages*MAX_GH_LEVEL) {
            pages++;
        }
        for(int i=0; i!=current_page*MAX_GH_LEVEL; i++){
            result->NextRow();
        }

        uint32 i = 0;
        while(i < MAX_GH_LEVEL) {
            Field *fields = result->Fetch();
            guildhouseId = fields[0].GetInt32();
            name = fields[1].GetString();
            name += " (";
            name += fields[2].GetString();
            name += " lvl).";

            pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, name, GOSSIP_SENDER_MAIN, guildhouseId + NUMBER_TO_ID);

            if(!result->NextRow()) { break;}
            i++;
        }

        if (current_page != pages-1)
        {
            pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_NEXT_PAGE, GOSSIP_SENDER_MAIN, ACTION_NEXT_PAGE);
        }
        if (current_page != 0)
        {
            pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_PREV_PAGE, GOSSIP_SENDER_MAIN, ACTION_PREV_PAGE);
        }
        pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_NO, GOSSIP_SENDER_MAIN, ACTION_OK);
        pPlayer->SEND_GOSSIP_MENU(54730, pCreature->GetGUID());
        return true;
    } else {
        if (showFromId = 0)
        {
            pCreature->MonsterWhisper(WHISPER_NOTHING_GH, pPlayer->GetGUID());
        } else {
            ShowBuyList(pPlayer, pCreature, 0);
        }
    }
    return false;
}

void BuyGuildhouse(Player *pPlayer, Creature *pCreature, uint32 guildhouseId)
{
    int32 cost = 0;
    QueryResult result = CharacterDatabase.PQuery("SELECT guild, gh_level FROM guildhouses WHERE entry = '%u'", guildhouseId);
    if (result)
    {
        Field *fields = result->Fetch();

        uint32 guild = fields[0].GetUInt32();
        uint32 level = fields[1].GetUInt32();

        if(guild != 0)
        {
            return;
        }

        switch (level)
        {
            case 1:
                cost = GH_COST_LEVEL_1;
                break;
            case 2:
                cost = GH_COST_LEVEL_2;
                break;
            case 3:
                cost = GH_COST_LEVEL_3;
                break;
            case 4:
                cost = GH_COST_LEVEL_4;
                break;
            default:
                pPlayer->CLOSE_GOSSIP_MENU();
                return;
        }

    }
    if (pPlayer->GetMoney() >= cost)
    {
        CharacterDatabase.PExecute("UPDATE guildhouses SET status = '1', guild = '%u', next_buy = '0' WHERE entry = '%u'", pPlayer->GetGuildId(), guildhouseId);
        pCreature->MonsterWhisper(WHISPER_CONGRATULATION_BUY, pPlayer->GetGUID());
        pPlayer->ModifyMoney(-cost);
    } else {
        pCreature->MonsterWhisper(WHISPER_NO_MONEY, pPlayer->GetGUID());
        pPlayer->SendBuyError( BUY_ERR_NOT_ENOUGHT_MONEY, 0, 0, 0);
        return;
    }
}

void TeleToGH(Player *pPlayer, Creature *pCreature)
{
    QueryResult result = CharacterDatabase.PQuery("SELECT map, coord_x, coord_y, coord_z, coord_o FROM guildhouses WHERE guild = '%u'", pPlayer->GetGuildId());
    if (!result)
    {
        pCreature->MonsterWhisper(WHISPER_NOT_GH_BUY, pPlayer->GetGUID());
    } else {
        Field *fields = result->Fetch();
        uint32 MapId = fields[0].GetUInt32();
        float coord_X = fields[1].GetFloat();
        float coord_Y = fields[2].GetFloat();
        float coord_Z = fields[3].GetFloat();
        float coord_O = fields[4].GetFloat();

        pCreature->MonsterWhisper(WHISPER_YOU_ARE_TELEPORTING_TO_GH, pPlayer->GetGUID());
        pPlayer->TeleportTo(MapId, coord_X, coord_Y, coord_Z, coord_O);
    }
}

void SellGH(Player *pPlayer, Creature *pCreature)
{
    QueryResult result = CharacterDatabase.PQuery("SELECT gh_level FROM guildhouses WHERE guild = '%u'", pPlayer->GetGuildId());
    if (result)
    {
        Field *fields = result->Fetch();
        uint32 level = fields[0].GetUInt32();

        switch (level)
        {
            case 1:
                pPlayer->ModifyMoney(GH_COST_LEVEL_1 / 2);
                break;
            case 2:
                pPlayer->ModifyMoney(GH_COST_LEVEL_2 / 2);
                break;
            case 3:
                pPlayer->ModifyMoney(GH_COST_LEVEL_3 / 2);
                break;
            case 4:
                pPlayer->ModifyMoney(GH_COST_LEVEL_4 / 2);
                break;
            default:
                return;
        }
    }
    CharacterDatabase.PExecute("UPDATE guildhouses SET status = '0', guild = '0', next_buy = '1' WHERE guild = '%u'", pPlayer->GetGuildId());
    pCreature->MonsterWhisper(WHISPER_SALE_GH, pPlayer->GetGUID());
}

void UpgradeGH(Player *pPlayer, Creature *pCreature)
{
    QueryResult result = CharacterDatabase.PQuery("SELECT gh_level FROM guildhouses WHERE guild = '%u'", pPlayer->GetGuildId());
    if (result)
    {
        Field *fields = result->Fetch();
        uint32 level = fields[0].GetUInt32();
        int32 cost = 0;

        switch(level)
        {
            case 1: cost = GH_UPDATE_TO_LVL_2; break;
            case 2: cost = GH_UPDATE_TO_LVL_3; break;
            case 3: cost = GH_UPDATE_TO_LVL_4; break;
            case 4:
                pCreature->MonsterWhisper(WHISPER_HIGH_LEVEL, pPlayer->GetGUID());
                return;
            default: return;
        }

        bool canBuy = pPlayer->HasEnoughMoney(cost);

        if (!canBuy) {
            pCreature->MonsterWhisper(WHISPER_NO_MONEY, pPlayer->GetGUID());
            pPlayer->SendBuyError( BUY_ERR_NOT_ENOUGHT_MONEY, 0, 0, 0);
            return;
        }

        pPlayer->ModifyMoney(-cost);
        CharacterDatabase.PExecute("UPDATE guildhouses SET gh_level = '%u' WHERE guild = '%u'", ++level, pPlayer->GetGuildId());
    }

    pCreature->MonsterWhisper(WHISPER_CONGRATULATION_LEVEL, pPlayer->GetGUID());
}

void BuyKey(Player *pPlayer, Creature *pCreature)
{
    QueryResult result = CharacterDatabase.PQuery("SELECT gh_level FROM guildhouses WHERE guild = '%u'", pPlayer->GetGuildId());
    if (result)
    {
        Field *fields = result->Fetch();
        uint32 level = fields[0].GetUInt32();
        if (level >= 2) {
            if (!pPlayer->HasItemCount(38261, 1))
            {
                pPlayer->GetSession()->SendListInventory(pCreature->GetGUID());
            } else {
                pCreature->MonsterWhisper(WHISPER_ALREADY_HAS_KEY, pPlayer->GetGUID());
                return;
            }
            pCreature->MonsterWhisper(WHISPER_BUY_KEY, pPlayer->GetGUID()); return;
        } else {
            pCreature->MonsterWhisper(WHISPER_LOW_GH_LEVEL, pPlayer->GetGUID());
            return;
        }
    } else {
        pCreature->MonsterWhisper(WHISPER_NOT_GH_BUY, pPlayer->GetGUID());
        return;
    }
}

void StartGH(Player *pPlayer, Creature *pCreature)
{
    pages=0;
    current_page=0;
    uint32 guildhouselvl = 0;

    QueryResult result_guild = CharacterDatabase.PQuery("SELECT entry, gh_level FROM guildhouses WHERE guild = '%u'", pPlayer->GetGuildId());
    QueryResult result_ghouse = CharacterDatabase.PQuery("SELECT entry FROM guildhouses WHERE next_buy = '1'");

        pPlayer->PlayerTalkClass->ClearMenus();

    if(result_guild) {
        Field *fields = result_guild->Fetch();
        guildhouselvl = fields[1].GetInt32();
    }

    if(pPlayer->GetGuildId() >= 1 && pPlayer->GetRank() == 0 && !result_guild && result_ghouse)  // its buy
        pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_BUY_GH, GOSSIP_SENDER_MAIN, ACTION_BUY);

    /*if(pPlayer->GetGuildId() >= 1 && pPlayer->GetRank() == 0 && result_guild)   // its sale
        pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_SALE_GH, GOSSIP_SENDER_MAIN, ACTION_SELL);*/

    if(pPlayer->GetGuildId() >= 1 && pPlayer->GetRank() == 0 && result_guild)   // its upgrade
        pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_UPGRADE_GH, GOSSIP_SENDER_MAIN, ACTION_UPGRADE);

    if(pPlayer->GetGuildId() >= 1 && result_guild)   // teleport
        pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_TELEPORT_TO_GH, GOSSIP_SENDER_MAIN, ACTION_TELEPORT);

    if(pPlayer->GetGuildId() >= 1 && guildhouselvl >= 2) {  // tele key
        pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_VENDOR, GOSSIP_BUY_TELE_KEY, GOSSIP_SENDER_MAIN, ACTION_BUY_KEY);
    }

    pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_GOODBYE, GOSSIP_SENDER_MAIN, ACTION_GOODBYE);
    pPlayer->SEND_GOSSIP_MENU(54730, pCreature->GetGUID());
}

void TeleMenuMainCity(Player *pPlayer, Creature *pCreature)
{
    QueryResult result = CharacterDatabase.PQuery("SELECT gh_level FROM guildhouses WHERE guild = '%u'", pPlayer->GetGuildId());
    if (result)
    {
        Field *fields = result->Fetch();
        uint32 level = fields[0].GetUInt32();
        if (level == 3)
        {
            if (pPlayer->GetTeam() == ALLIANCE)
            {
                pPlayer->ADD_GOSSIP_ITEM(5, "Darnassus."        , GOSSIP_SENDER_MAIN, 1203);
                pPlayer->ADD_GOSSIP_ITEM(5, "Exodar."            , GOSSIP_SENDER_MAIN, 1216);
                pPlayer->ADD_GOSSIP_ITEM(5, "Stormwind."        , GOSSIP_SENDER_MAIN, 1206);
                pPlayer->ADD_GOSSIP_ITEM(5, "Ironforge."        , GOSSIP_SENDER_MAIN, 1224);
                pPlayer->ADD_GOSSIP_ITEM(5, "Shattrath City."           , GOSSIP_SENDER_MAIN, 1287);
                pPlayer->ADD_GOSSIP_ITEM(5, "Dalaran."                  , GOSSIP_SENDER_MAIN, 1205);
                pPlayer->ADD_GOSSIP_ITEM(5, "Isle Of Quel'Danas."       , GOSSIP_SENDER_MAIN, 1288);
                pPlayer->ADD_GOSSIP_ITEM(7, "[Instances] ->"        , GOSSIP_SENDER_MAIN, 5550);
                pPlayer->ADD_GOSSIP_ITEM(7, "[Instances WotLK] ->"    , GOSSIP_SENDER_MAIN, 5554);
            } else {
                pPlayer->ADD_GOSSIP_ITEM(5, "Orgrimmar."        , GOSSIP_SENDER_MAIN, 1215);
                pPlayer->ADD_GOSSIP_ITEM(5, "Silvermoon."        , GOSSIP_SENDER_MAIN, 1217);
                pPlayer->ADD_GOSSIP_ITEM(5, "Undercity."        , GOSSIP_SENDER_MAIN, 1213);
                pPlayer->ADD_GOSSIP_ITEM(5, "Thunder Bluff."            , GOSSIP_SENDER_MAIN, 1225);
                pPlayer->ADD_GOSSIP_ITEM(5, "Shattrath City."           , GOSSIP_SENDER_MAIN, 1287);
                pPlayer->ADD_GOSSIP_ITEM(5, "Dalaran."                  , GOSSIP_SENDER_MAIN, 1205);
                pPlayer->ADD_GOSSIP_ITEM(5, "Isle Of Quel'Danas."       , GOSSIP_SENDER_MAIN, 1288);
                pPlayer->ADD_GOSSIP_ITEM(7, "[Instances] ->"        , GOSSIP_SENDER_MAIN, 5550);
                pPlayer->ADD_GOSSIP_ITEM(7, "[Instances WotLK] ->"    , GOSSIP_SENDER_MAIN, 5554);
            }
            pPlayer->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, pCreature->GetGUID()); return;
        } else {
            pCreature->MonsterWhisper(WHISPER_LOW_GH_LEVEL, pPlayer->GetGUID());
            pPlayer->CLOSE_GOSSIP_MENU();
            return;
        }
    } else {
        pCreature->MonsterWhisper(WHISPER_NOT_GH_BUY, pPlayer->GetGUID());
        pPlayer->CLOSE_GOSSIP_MENU();
        return;
    }
}

class npc_gh_keeper : public CreatureScript
{
    public:
    npc_gh_keeper() : CreatureScript("npc_gh_keeper") { }

    bool OnGossipHello(Player* pPlayer, Creature* pCreature)
    {
        StartGH(pPlayer, pCreature);
        return true;
    }

    bool OnGossipSelect(Player* pPlayer, Creature* pCreature, uint32 /*uiSender*/, uint32 action)
    {
        switch (action)
        {
            case ACTION_BUY:
                ShowBuyList(pPlayer, pCreature);
                break;
            case ACTION_SELL:
                pPlayer->PlayerTalkClass->ClearMenus();
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_SALE_GH_READY, GOSSIP_SENDER_MAIN, ACTION_SELL_READY);
                pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_NO, GOSSIP_SENDER_MAIN, ACTION_OK);
                pPlayer->SEND_GOSSIP_MENU(54732, pCreature->GetGUID());
                break;
            case ACTION_UPGRADE:
                UpgradeGH(pPlayer, pCreature);
                pPlayer->CLOSE_GOSSIP_MENU();
                break;
            case ACTION_TELEPORT:
                TeleToGH(pPlayer, pCreature);
                pPlayer->CLOSE_GOSSIP_MENU();
                break;
            /*case ACTION_SELL_READY:
                SellGH(pPlayer, pCreature);
                pPlayer->CLOSE_GOSSIP_MENU();
                break;*/
            case ACTION_BUY_KEY:
                BuyKey(pPlayer, pCreature);
                pPlayer->CLOSE_GOSSIP_MENU();
                break;
            case ACTION_GOODBYE:
                pPlayer->CLOSE_GOSSIP_MENU();
                break;
            case ACTION_OK:
                StartGH(pPlayer, pCreature);
                break;
            case ACTION_NEXT_PAGE:
                current_page++;
                ShowBuyList(pPlayer, pCreature);
                break;
            case ACTION_PREV_PAGE:
                current_page--;
                ShowBuyList(pPlayer, pCreature);
                break;
            default: {
                if(action > NUMBER_TO_ID) {
                    pPlayer->CLOSE_GOSSIP_MENU();
                    BuyGuildhouse(pPlayer, pCreature, action - NUMBER_TO_ID);
                }
            };
                break;
        }
        return true;
    }
};

class npc_tele_guy : public CreatureScript
{
    public:
    npc_tele_guy() : CreatureScript("npc_tele_guy") { }

    bool OnGossipHello(Player* pPlayer, Creature* pCreature)
    {
        TeleMenuMainCity(pPlayer, pCreature);
        return true;
    }

    bool OnGossipSelect(Player* pPlayer, Creature* pCreature, uint32 /*uiSender*/, uint32 action)
    {
        if(pPlayer->isInCombat())
        {
            pPlayer->CLOSE_GOSSIP_MENU();
            pCreature->MonsterWhisper("You are in combat!", pPlayer->GetGUID());
            return false;
        }
        if(pPlayer->getLevel() < 80)
        {
        pPlayer->CLOSE_GOSSIP_MENU();
            pCreature->MonsterWhisper("You must have 80 level", pPlayer->GetGUID());
            return false;
        }

        if(pPlayer->GetMoney() < GH_COST_TELEPORT)
        {
            pPlayer->CLOSE_GOSSIP_MENU();
            pCreature->MonsterWhisper("You haven't enough money", pPlayer->GetGUID());
            pPlayer->SendBuyError(BUY_ERR_NOT_ENOUGHT_MONEY, 0, 0, 0);
            return false;
        }

        switch(action)
        {
            case 5550: //Instances
                pPlayer->ADD_GOSSIP_ITEM(5, "The Wailing Caverns."     , GOSSIP_SENDER_MAIN, 1249);
                pPlayer->ADD_GOSSIP_ITEM(5, "Deadmines."               , GOSSIP_SENDER_MAIN, 1250);
                pPlayer->ADD_GOSSIP_ITEM(5, "Shadowfang Keep."         , GOSSIP_SENDER_MAIN, 1251);
                pPlayer->ADD_GOSSIP_ITEM(5, "Blackfathom Deeps."       , GOSSIP_SENDER_MAIN, 1252);
                pPlayer->ADD_GOSSIP_ITEM(5, "Razorfen Kraul."          , GOSSIP_SENDER_MAIN, 1254);
                pPlayer->ADD_GOSSIP_ITEM(5, "Razorfen Downs."          , GOSSIP_SENDER_MAIN, 1256);
                pPlayer->ADD_GOSSIP_ITEM(5, "Scarlet Monastery."       , GOSSIP_SENDER_MAIN, 1257);
                pPlayer->ADD_GOSSIP_ITEM(7, "[More] ->"           , GOSSIP_SENDER_MAIN, 5551);
                pPlayer->ADD_GOSSIP_ITEM(7, "<- [Main Menu]"       , GOSSIP_SENDER_MAIN, 5552);
                pPlayer->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, pCreature->GetGUID());
                break;
            case 5551: //More Instances
                pPlayer->ADD_GOSSIP_ITEM(5, "Uldaman."                 , GOSSIP_SENDER_MAIN, 1258);
                pPlayer->ADD_GOSSIP_ITEM(5, "Zul'Farrak."              , GOSSIP_SENDER_MAIN, 1259);
                pPlayer->ADD_GOSSIP_ITEM(5, "Maraudon."                , GOSSIP_SENDER_MAIN, 1260);
                pPlayer->ADD_GOSSIP_ITEM(5, "Maraudon."                , GOSSIP_SENDER_MAIN, 1260);
                pPlayer->ADD_GOSSIP_ITEM(5, "The Sunken Temple."       , GOSSIP_SENDER_MAIN, 1261);
                pPlayer->ADD_GOSSIP_ITEM(5, "Blackrock Depths."        , GOSSIP_SENDER_MAIN, 1262);
                pPlayer->ADD_GOSSIP_ITEM(5, "Dire Maul."               , GOSSIP_SENDER_MAIN, 1263);
                pPlayer->ADD_GOSSIP_ITEM(5, "Blackrock Spire."         , GOSSIP_SENDER_MAIN, 1264);
                pPlayer->ADD_GOSSIP_ITEM(5, "Stratholme."              , GOSSIP_SENDER_MAIN, 1265);
                pPlayer->ADD_GOSSIP_ITEM(5, "Scholomance."             , GOSSIP_SENDER_MAIN, 1266);
                pPlayer->ADD_GOSSIP_ITEM(7, "[More] ->"           , GOSSIP_SENDER_MAIN, 5553);
                pPlayer->ADD_GOSSIP_ITEM(7, "<- [Back]"           , GOSSIP_SENDER_MAIN, 5550);
                pPlayer->ADD_GOSSIP_ITEM(7, "<- [Main Menu]"       , GOSSIP_SENDER_MAIN, 5552);
                pPlayer->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, pCreature->GetGUID());
                break;
            case 5553: //Instances 60-70
                pPlayer->ADD_GOSSIP_ITEM(5, "Karazhan."                , GOSSIP_SENDER_MAIN, 4007);
                pPlayer->ADD_GOSSIP_ITEM(5, "Gruul's Lair."            , GOSSIP_SENDER_MAIN, 4008);
                pPlayer->ADD_GOSSIP_ITEM(5, "Hellfire Citadel."        , GOSSIP_SENDER_MAIN, 4009);
                pPlayer->ADD_GOSSIP_ITEM(5, "Coilfang Reservoir."      , GOSSIP_SENDER_MAIN, 4010);
                pPlayer->ADD_GOSSIP_ITEM(5, "Tempest Keep."            , GOSSIP_SENDER_MAIN, 4011);
                pPlayer->ADD_GOSSIP_ITEM(5, "Caverns of Time."         , GOSSIP_SENDER_MAIN, 4012);
                pPlayer->ADD_GOSSIP_ITEM(5, "Zul'Aman."                , GOSSIP_SENDER_MAIN, 4016);
                pPlayer->ADD_GOSSIP_ITEM(5, "Black Temple."            , GOSSIP_SENDER_MAIN, 4013);
                pPlayer->ADD_GOSSIP_ITEM(5, "Magister's Terrace."      , GOSSIP_SENDER_MAIN, 4017);
                pPlayer->ADD_GOSSIP_ITEM(5, "Sunwell Plateau."         , GOSSIP_SENDER_MAIN, 4018);
                pPlayer->ADD_GOSSIP_ITEM(7, "<- [Back]"           , GOSSIP_SENDER_MAIN, 5550);
                pPlayer->ADD_GOSSIP_ITEM(7, "<- [Main Menu]"       , GOSSIP_SENDER_MAIN, 5552);
                pPlayer->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, pCreature->GetGUID());
                break;
            case 5554: //Instances 75-80 NORTHREND
                pPlayer->ADD_GOSSIP_ITEM(5, "Utgarde Keep."            , GOSSIP_SENDER_MAIN, 4019);
                pPlayer->ADD_GOSSIP_ITEM(5, "The Nexus."               , GOSSIP_SENDER_MAIN, 4020);
                pPlayer->ADD_GOSSIP_ITEM(5, "Azjol-Nerub."             , GOSSIP_SENDER_MAIN, 4021);
                pPlayer->ADD_GOSSIP_ITEM(5, "Ahn'kahet."               , GOSSIP_SENDER_MAIN, 4022);
                pPlayer->ADD_GOSSIP_ITEM(5, "Drak'Tharon Keep."        , GOSSIP_SENDER_MAIN, 4023);
                pPlayer->ADD_GOSSIP_ITEM(5, "The Violet Hold."         , GOSSIP_SENDER_MAIN, 4024);
                pPlayer->ADD_GOSSIP_ITEM(5, "Gun' Drak."               , GOSSIP_SENDER_MAIN, 4025);
                pPlayer->ADD_GOSSIP_ITEM(5, "Utgarde Pinnacle."        , GOSSIP_SENDER_MAIN, 4026);
                pPlayer->ADD_GOSSIP_ITEM(5, "Ulduar."                  , GOSSIP_SENDER_MAIN, 4027);
                pPlayer->ADD_GOSSIP_ITEM(5, "The Obsidian Sanctum."    , GOSSIP_SENDER_MAIN, 4028);
                pPlayer->ADD_GOSSIP_ITEM(5, "Naxxramas."               , GOSSIP_SENDER_MAIN, 4029);
                pPlayer->ADD_GOSSIP_ITEM(7, "<- [Back]"           , GOSSIP_SENDER_MAIN, 5550);
                pPlayer->ADD_GOSSIP_ITEM(7, "<- [Main Menu]"       , GOSSIP_SENDER_MAIN, 5552);
                pPlayer->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, pCreature->GetGUID());
                break;
            case 5552: //Back To Main Menu
                TeleMenuMainCity(pPlayer, pCreature);
                break;

            // Main City Tele stuff
            case 1203: // Teleport to Darnassus
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-1*GH_COST_TELEPORT);
                pPlayer->TeleportTo(1, 9947.52f, 2482.73f, 1316.21f, 0.0f);
                break;
            case 1206: // Teleport to Stormwind
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-1*GH_COST_TELEPORT);
                pPlayer->TeleportTo(0, -8960.14f, 516.266f, 96.3568f, 0.0f);
                break;
            case 1205: // Teleport to Dalaran
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-1*GH_COST_TELEPORT);
                pPlayer->TeleportTo(571, 5804.14f, 624.770f, 647.7670f, 1.64f);
                break;
            case 1213: // Teleport to Undercity
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-1*GH_COST_TELEPORT);
                pPlayer->TeleportTo(0, 1819.71f, 238.79f, 60.5321f, 0.0f);
                break;
            case 1215: // Teleport to Orgrimmar
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-1*GH_COST_TELEPORT);
                pPlayer->TeleportTo(1, 1552.5f, -4420.66f, 8.94802f, 0.0f);
                break;
            case 1216: // Teleport to Exodar
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-1*GH_COST_TELEPORT);
                pPlayer->TeleportTo(530, -4073.03f, -12020.4f, -1.47f, 0.0f);
                break;
            case 1217: // Teleport to Silvermoon
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-1*GH_COST_TELEPORT);
                pPlayer->TeleportTo(530, 9338.74f, -7277.27f, 13.7895f, 0.0f);
                break;
            case 1222: // Teleport to Gnomeregan
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-1*GH_COST_TELEPORT);
                pPlayer->TeleportTo(0, -5163.43f,660.40f,348.28f,4.65f);
                break;
            case 1224: // Teleport to Ironforge
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-1*GH_COST_TELEPORT);
                pPlayer->TeleportTo(0, -4924.07f, -951.95f, 501.55f, 5.40f);
                break;
            case 1225: // Teleport to Thunder Bluff
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-1*GH_COST_TELEPORT);
                pPlayer->TeleportTo(1, -1280.19f,127.21f,131.35f,5.16f);
                break;

            // Dungeons/Raids Tele stuff - double price
            case 1249: // Teleport to the Wailing Caverns
            pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
            pPlayer->TeleportTo(1, -722.53f,-2226.30f,16.94f,2.71f);
                break;
            case 1250: // Teleport to the Deadmines
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
            pPlayer->TeleportTo(0, -11212.04f,1658.58f,25.67f,1.45f);
                break;
            case 1251: // Teleport to Shadowfang Keep
            pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
            pPlayer->TeleportTo(0, -254.47f,1524.68f,76.89f,1.56f);
                break;
            case 1252: // Teleport to Blackfathom Deeps
            pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
            pPlayer->TeleportTo(1, 4254.58f,664.74f,-29.04f,1.97f);
                break;
            case 1254: // Teleport to Razorfen Kraul
            pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
            pPlayer->TeleportTo(1, -4484.04f,-1739.40f,86.47f,1.23f);
                break;
            case 1255: // Teleport to Gnomeregan
            pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
            pPlayer->TeleportTo(0, -5162.62f,667.81f,248.05f,1.48f);
                break;
            case 1256: // Teleport to Razorfen Downs
            pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
            pPlayer->TeleportTo(1, -4645.08f,-2470.85f,85.53f,4.39f);
                break;
            case 1257: // Teleport to the Scarlet Monastery
            pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
            pPlayer->TeleportTo(0, 2843.89f,-693.74f,139.32f,5.11f);
                break;
            case 1258: // Teleport to Uldaman
            pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
            pPlayer->TeleportTo(0, -6119.70f,-2957.30f,204.11f,0.03f);
                break;
            case 1259: // Teleport to Zul'Farrak
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(1, -6839.39f,-2911.03f,8.87f,0.41f);
                break;
            case 1260: // Teleport to Maraudon
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(1, -1433.33f,2955.34f,96.21f,4.82f);
                break;
            case 1261: // Teleport to the Sunken Temple
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(0, -10346.92f,-3851.90f,-43.41f,6.09f);
                break;
            case 1262: // Teleport to Blackrock Depths
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(0, -7301.03f,-913.19f,165.37f,0.08f);
                break;
            case 1263: // Teleport to Dire Maul
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(1, -3982.47f,1127.79f,161.02f,0.05f);
                break;
            case 1264: // Teleport to Blackrock Spire
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(0, -7535.43f,-1212.04f,285.45f,5.29f);
                break;
            case 1265: // Teleport to Stratholme
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(0, 3263.54f,-3379.46f,143.59f,0.00f);
                break;
            case 1266: // Teleport to Scholomance
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(0, 1219.01f,-2604.66f,85.61f,0.50f);
                break;
            case 1287: // Teleport to Shattrath City
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(530, -1850.209961f, 5435.821777f, -10.961435f, 3.403913f);
                break;
            case 1288: // Teleport to Isle Of Quel'Danas
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(530, 12947.4f,-6893.31f,5.68398f,3.09154f);
                break;
            case 4007: // Teleport to Karazhan
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(0, -11118.8f, -2010.84f, 47.0807f, 0.0f);
                break;
            case 4008: // Teleport to Gruul's Lair
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(530, 3539.007568f, 5082.357910f, 1.691071f, 0.0f);
                break;
            case 4009: // Teleport to Hellfire Citadel
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(530, -305.816223f, 3056.401611f, -2.473183f, 2.01f);
                break;
            case 4010: // Teleport to Coilfang Reservoir
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(530, 517.288025f, 6976.279785f, 32.007198f, 0.0f);
                break;
            case 4011: // Teleport to Tempest Keep
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(530, 3089.579346f, 1399.046509f, 187.653458f, 4.794070f);
                break;
            case 4012: // Teleport to Caverns of Time
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(1, -8173.66f, -4746.36f, 33.8423f, 4.93989f);
                break;
            case 4016: // Teleport to Zul'Aman
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(530, 6846.95f, -7954.5f, 170.028f, 4.61501f);
                break;
            case 4013: // Teleport to Black Temple
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(530, -3610.719482f, 324.987579f, 37.400028f, 3.282981f);
                break;
            case 4017: // Teleport to Magister Terrace
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(530, 12884.6f, -7317.69f, 65.5023f, 4.799f);
                break;
            case 4018: // Teleport to Sunwell
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(530, 12574.1f, -6774.81f, 15.0904f, 3.13788f);
                break;
            case 4019: // Teleport to Utgarde Keep
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(571, 1219.720f, -4865.28f, 41.25f, 0.31f);
                break;
            case 4020: // Teleport to The Nexus
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(571, 3776.950f, 6953.80f, 105.05f, 0.345f);
                break;
            case 4021: // Teleport to Azjol-Nerub
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(571, 3675.430f, 2169.00f, 35.90f, 2.29f);
                break;
            case 4022: // Teleport to Ahn'kahet: The Old Kingdom
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(571, 3646.760f, 2045.17f, 1.79f, 4.37f);
                break;
            case 4023: // Teleport to Drak'Tharon Keep
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(571, 4450.860f, -2045.25f, 162.83f, 0.00f);
                break;
            case 4024: // Teleport to The Violet Hold
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(571, 5679.820f, 486.80f, 652.40f, 4.08f);
                break;
            case 4025: // Teleport to GunDrak
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(571, 6937.540f, -4455.98f, 450.68f, 1.00f);
                break;
            case 4026: // Teleport to Utgarde Pinnacle
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(571, 1245.690f, -4856.59f, 216.86f, 3.45f);
                break;
            case 4027: // Teleport to Ulduar
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(571, 8976.240f, -1281.33f, 1059.01f, 0.58f);
                break;
            case 4028: // Teleport to The Obsidian Sanctum
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(571, 3625.780f, 280.40f, -120.14f, 3.25f);
                break;
            case 4029: // Teleport to Naxxramas
                pPlayer->CLOSE_GOSSIP_MENU();
                pPlayer->ModifyMoney(-2*GH_COST_TELEPORT);
                pPlayer->TeleportTo(571, 3668.719f, -1262.460f, 243.63f, 5.03f);
                break;
        }
        return true;
    }
};

class item_gh_tele : public ItemScript
{
    public:
    item_gh_tele() : ItemScript("item_gh_tele") { }

    bool OnUse(Player* pPlayer, Item* pItem, SpellCastTargets const& targets)
    {
        if(pPlayer->isInCombat())
            return true;

        if (QueryResult result = CharacterDatabase.PQuery("SELECT gh_level, map, coord_x, coord_y, coord_z, coord_o FROM guildhouses WHERE guild = '%u'", pPlayer->GetGuildId()))
        {
            if (!result)
                return true;
            else {
                Field *fields = result->Fetch();
                uint32 ghLevel = fields[0].GetUInt32();
                uint32 MapId  = fields[1].GetUInt32();
                float coord_X = fields[2].GetFloat();
                float coord_Y = fields[3].GetFloat();
                float coord_Z = fields[4].GetFloat();
                float coord_O = fields[5].GetFloat();
                if (ghLevel < 2)
                    return true;
                pPlayer->TeleportTo(MapId, coord_X, coord_Y, coord_Z, coord_O);
            }
        }
        return false;
    }
};

void AddSC_guild_house_keeper()
{
    new npc_gh_keeper();
    new npc_tele_guy();
    new item_gh_tele();
}
